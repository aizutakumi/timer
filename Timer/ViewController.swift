//
//  ViewController.swift
//  Timer
//
//  Created by 会津匠 on 2016/07/07.
//  Copyright © 2016年 会津匠. All rights reserved.
//

import UIKit
import AudioToolbox

class ViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var timerPickerView: UIPickerView!
    
    @IBOutlet weak var minute_time: UILabel!    //分
    @IBOutlet weak var second_time: UILabel!    //秒
    
    @IBOutlet weak var history_minute_time1: UILabel!   //履歴１の分
    @IBOutlet weak var history_second_time1: UILabel!   //履歴１の秒
    
    @IBOutlet weak var history_minute_time2: UILabel!   //履歴２の分
    @IBOutlet weak var history_second_time2: UILabel!   //履歴２の秒
    
    @IBOutlet weak var history_minute_time3: UILabel!   //履歴３の分
    @IBOutlet weak var history_second_time3: UILabel!   //履歴３の秒
    
    var count = 0
    var timer_flg = false   //タイマーが動いているかのフラグ
    var timer = Timer()
    let dataList = [[Int](0...59), [Int](0...59)]
    
    var now_time = 0
    var history1 = 0
    var history2 = 0
    var history3 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //コンポーネントの個数を返すメソッド
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return dataList.count
    }
    
    //コンポーネントに含まれるデータの個数を返すメソッド
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataList[component].count
    }
    
    //データを返すメソッド
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(format: "%02d", dataList[component][row])
    }
    
    //データ選択時の呼び出しメソッド
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //コンポーネントごとに現在選択されているデータを取得する。
        let data1 = self.pickerView(pickerView, titleForRow: pickerView.selectedRow(inComponent: 0), forComponent: 0)
        let data2 = self.pickerView(pickerView, titleForRow: pickerView.selectedRow(inComponent: 1), forComponent: 1)
        
        //タイマーが動いてる時はラベルを変えない
        if !timer_flg{
            minute_time.text = String(format: "%02d", Int(data1!)!)
            second_time.text = String(format: "%02d", Int(data2!)!)
        }
    }
    
    @IBAction func start(_ sender: AnyObject) {
        //タイマースタート
        //フラグがオフかつタイムが０より大きい
        count = Int(minute_time.text!)! * 60 + Int(second_time.text!)!
        if !timer_flg && count > 0 {
            timer = Timer.scheduledTimer(
                timeInterval: 1.0,    //１秒間隔
                target:self,    //タイマー処理の場所
                selector:#selector(ViewController.countDown(_:)),  //処理
                userInfo:nil,
                repeats:true)   //繰り返し
        
            timer_flg = true
            
            setHistoryTime(sender)
        }
    }
    
    @IBAction func stop(_ sender: AnyObject) {
        //タイマーストップ
        timer.invalidate()
        timer_flg = false
    }
    
    @IBAction func reset(_ sender: AnyObject) {
        //タイマーリセット
        if !timer_flg {
            minute_time.text = "00"
            second_time.text = "00"
        }
    }
    
    func countDown(_ sender: AnyObject){
        //カウントダウン処理
        if count >= 60 {
            count-=1
            
            minute_time.text = String(format: "%02d", Int(count / 60))
            second_time.text = String(format: "%02d", Int(count % 60))
        }
            
        else if count < 60{
            count-=1
            
            minute_time.text = "00"
            second_time.text = String(format: "%02d", count)
            if count == 0{
                sender.invalidate()
            }
        }
        
        if count <= 0 {
            if timer_flg {
                //音を鳴らす
                AudioServicesPlaySystemSound(1009);
            
                //カウントの表示を戻す
                minute_time.text = String(format: "%02d", Int(now_time / 60))
                second_time.text = String(format: "%02d", Int(now_time % 60))
            }
            
            stop(sender)
        }
    }
    
    func setHistoryTime(_ sender: AnyObject){
        //入力したタイムを記憶
        now_time = count
        //履歴２があれば履歴３に移動
        if(history1 != 0){
            history_minute_time3.text = String(format: "%02d", Int(history2 / 60))
            history_second_time3.text = String(format: "%02d", Int(history2 % 60))
            history3 = history2
        }
        //履歴１があれば履歴２に移動
        if(history1 != 0){
            history_minute_time2.text = String(format: "%02d", Int(history1 / 60))
            history_second_time2.text = String(format: "%02d", Int(history1 % 60))
            history2 = history1
        }
        //履歴１に記録
        history_minute_time1.text = String(format: "%02d", Int(now_time / 60))
        history_second_time1.text = String(format: "%02d", Int(now_time % 60))
        history1 = now_time
    }
    
    @IBAction func getHistorytime1(_ sender: AnyObject) {
        //履歴１の取り出し
        if(history1 != 0){
            minute_time.text = String(format: "%02d", Int(history1 / 60))
            second_time.text = String(format: "%02d", Int(history1 % 60))
        }
    }
    @IBAction func getHistorytime2(_ sender: AnyObject) {
        //履歴２の取り出し
        if(history2 != 0){
            minute_time.text = String(format: "%02d", Int(history2 / 60))
            second_time.text = String(format: "%02d", Int(history2 % 60))
        }
    }
    @IBAction func getHistorytime3(_ sender: AnyObject) {
        //履歴3の取り出し
        if(history3 != 0){
            minute_time.text = String(format: "%02d", Int(history3 / 60))
            second_time.text = String(format: "%02d", Int(history3 % 60))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

